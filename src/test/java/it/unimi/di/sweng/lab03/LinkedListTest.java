package it.unimi.di.sweng.lab03;

import static org.assertj.core.api.Assertions.*;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;

public class LinkedListTest {

	@Rule
    public Timeout globalTimeout = Timeout.seconds(2);

	private IntegerList list;
	
	@Test
	public void firstTestToBeReplaced() {
		list = new IntegerList();
		assertThat(list.toString()).isEqualTo("[]");
		
	}
	@Test
	public void addLastTEst() {
		list = new IntegerList();
		list.addLast(1);
		assertThat(list.toString()).isEqualTo("[1]");
		list.addLast(3);
		assertThat(list.toString()).isEqualTo("[1 3]");
	}
	
	@Test
	public void costruttoreStringTest() {
		list = new IntegerList("");
		assertThat(list.toString()).isEqualTo("[]");
		
		list = new IntegerList("11");
		assertThat(list.toString()).isEqualTo("[11]");
		list = new IntegerList("1 2 3");
		assertThat(list.toString()).isEqualTo("[1 2 3]");
	}
	@Test(expected=IllegalArgumentException.class)
	
	public void robustezzaCostruttoreStringTest() {
		
		list = new IntegerList("1 2 aaa");
		
	}
	@Test
	public void addFirstTest(){
		list = new IntegerList();
		list.addFirst(1);
		assertThat(list.toString()).isEqualTo("[1]");
		list.addFirst(3);
		assertThat(list.toString()).isEqualTo("[3 1]");
	}
	
	@Test
	public void removeFirstTest(){
		list = new IntegerList("1 2 3");
		list.removeFirst();
		assertThat(list.toString()).isEqualTo("[2 3]");
		//assertTrue("someLibraryMethod should return 'true'", list.removeFirst());
	}
	
	@Test
	public void removeLastTest(){
		list = new IntegerList("1 2 3");
		list.removeLast();
		assertThat(list.toString()).isEqualTo("[1 2]");
		//assertTrue("someLibraryMethod should return 'true'", list.removeFirst());
	}
	
	@Test
	public void removeTest(){
		list = new IntegerList("1 2 3 4 5");
		list.remove(2);
		assertThat(list.toString()).isEqualTo("[1 3 4 5]");
		list.remove(4);
		assertThat(list.toString()).isEqualTo("[1 3 5]");
	}
	
	@Test
	public void removeAllTest(){
		list = new IntegerList("1 2 4 2 4 5");
		list.removeAll(2);
		assertThat(list.toString()).isEqualTo("[1 4 4 5]");
		
		list = new IntegerList("1 2 2 2 4 5");
		list.removeAll(2);
		assertThat(list.toString()).isEqualTo("[1 4 5]");
	}
	
}
