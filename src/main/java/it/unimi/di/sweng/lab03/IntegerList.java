package it.unimi.di.sweng.lab03;

public class IntegerList {
	
	private IntegerNode head = null;
	private IntegerNode tail = null;
	
	public IntegerList(){}
	
	public IntegerList(String str) {
		if(!str.equals("")){			
			String vetString[] = str.split(" ");
			for(String s: vetString){
				int v = Integer.parseInt(s, 10);
				addLast(v);
			}
		}
		
	}

	public String toString(){
		String result = "[";
		IntegerNode currentNode = head;
		int i =0;
		while(currentNode != null){
			if(i++ > 0)
				result += " ";
			result += currentNode.getValue();
			currentNode = currentNode.next();
		}
		return result + "]";
	}

	public void addLast(int value) {
		if(head == null)
			head = tail = new IntegerNode(value);
		else{
			IntegerNode node = new IntegerNode(value);;
			tail.setNext(node);
			tail = node;
		}
	}

	public void addFirst(int s) {
	
		if(head == null)
			head = tail = new IntegerNode(s);
		else{
			IntegerNode node = new IntegerNode(s);;
			node.setNext(head);
			head = node;
		}
		
	}

	public boolean removeFirst() {
		if(head != null)
			head = head.next();
		else
			return false;
		
		return true;
	}

	public boolean removeLast() {
		if(tail != null){
			IntegerNode currentNode = head;
			while(currentNode.next() != tail){
				currentNode = currentNode.next();
			}
			currentNode.setNext(null);
			tail = currentNode;
		
		}else
			return false;
		
		return true;
		
	}

	public boolean remove(int key) {
		
		IntegerNode currentNode = head;
		boolean flag = false;
		
		if(head.getValue() == key){
			flag = removeFirst();
			return flag;
		}
		
		if(tail.getValue() == key){
			flag = removeLast();
			return flag;
		}
		
		while(currentNode.next() != tail && currentNode.next().getValue() != key){
			currentNode = currentNode.next();
		}
		
		if(currentNode.next().getValue() == key){
			currentNode.setNext(currentNode.next().next());
			flag = true;
		}
		return flag;
	}

	public boolean removeAll(int key) {
		IntegerNode currentNode = head;
		boolean flag = false;
		
		if(head.getValue() == key){
			flag = removeFirst();
			return flag;
		}
		
		if(tail.getValue() == key){
			flag = removeLast();
			return flag;
		}
		
		while(currentNode != tail){
			if(currentNode.next().getValue() == key)
				currentNode.setNext( searchNext( currentNode.next(), key) );
			currentNode = currentNode.next();
		}
		return flag;
	}
	
	private IntegerNode searchNext(IntegerNode node, int key){
		while( node != null){
			if( node.getValue() != key)
				return node;
			node = node.next();
		}
		return null;
	}
	
}
