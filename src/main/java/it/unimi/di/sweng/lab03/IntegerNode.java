package it.unimi.di.sweng.lab03;

public class IntegerNode {
	private Integer value;
	private IntegerNode next;
	
	IntegerNode(int n){
		value = n;
		next = null;
	}
	public IntegerNode next() {
		return next;
	}
	
	public void setNext(IntegerNode n) {
		next = n;
	}
	
	public Integer getValue(){
		
		return value;
	}
	
	
}
